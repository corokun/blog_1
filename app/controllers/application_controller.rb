class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :configure_permitted_params, if: :devise_controller?
  rescue_from CanCan::AccessDenied do |exception|
    # flash[:error] = "Access denied."
    # redirect_to root_path
    redirect_to root_path, :alert => exception.message
  end

  protected
  def configure_permitted_params
    devise_parameter_sanitizer.for(:account_update) {|u| u.permit(:name, :username, :email, :password, :password_confirmation, :current_password, :role)}
  end
end
