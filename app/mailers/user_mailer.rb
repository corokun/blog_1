class UserMailer < ActionMailer::Base
  default from: "no-reply@example.com"

  def notif_comment(toemail, comment)
    @toemail = toemail
    @article = comment.article
    @comment = comment
    @user = comment.user
    mail :to => @toemail, :subject => "Someone comment your article"
    # mail(:to => @toemail, :subject => "Someone comment your article")
  end
end
