class Ability
  include CanCan::Ability

  def initialize(user)
    alias_action :update, :destroy, :to => :ud
    alias_action :create, :read, :update, :destroy, :to => :crud

    can [:create, :read], :all
    can [:ud], Article do |article|
      article.try(:user) == user
    end
    can [:update], Comment do |comment|
      comment.try(:user) == user
    end
    can [:destroy], Comment do |comment|
      comment.try(:user) == user || comment.article.try(:user) == user
    end
  end
end
