class Article < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  belongs_to :user
  has_and_belongs_to_many :tags
  mount_uploader :image, ImageUploader

  validates :name, :content, presence: true
  ## validates_associated :users
  # validates :image, presence: true
  validates :user_id, presence: true
end
