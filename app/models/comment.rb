class Comment < ActiveRecord::Base
  belongs_to :article
  belongs_to :user

  validates :content, presence: true
  # validates_associated :user
  validates :user_id, presence: true

  def user_email
    self.user.email
  end
  def user_name
    self.user.name
  end
end
