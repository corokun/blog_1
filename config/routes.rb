Blog1::Application.routes.draw do
  resources :tags

  # resources :comments
  resources :articles do
    resources :comments
  end

  devise_for :users

  root 'articles#index'

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
end